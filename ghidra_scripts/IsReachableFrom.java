//TODO write a description for this script
//@author Ronman
//@category rmutil
//@keybinding 
//@menupath Tools.rmutil.IsReachableFrom
//@toolbar notarealimage.png


import generic.stl.Pair;
import ghidra.app.script.GhidraScript;
import ghidra.program.model.symbol.*;
import ghidra.program.model.listing.*;
import ghidra.program.model.address.*;
import java.util.*;

public class IsReachableFrom extends GhidraScript {

    public void run() throws Exception {
        // First, let's start by grabbing the current function...
        // We don't know if this is the destination or source yet, but we'll address that later...
        Function currentFunction = getFunctionContaining(currentAddress);

        if(currentFunction == null) {
            println("Failed: current address is not part of a function, cannot determine call parentage!");
            return;
        }
        println(String.format("Found current function: %s", currentFunction.getName()));

        String otherFunctionName = askString("Other Function", "Please enter another function name:");

        if (otherFunctionName.length() == 0) {
            println("No other function provided, nothing to do!");
            return;
        }

        /*
        Now we need to ask the direction...
         */
        ArrayList<String> options = new ArrayList<String>(2);
        options.add(String.format("%s >>>>> %s", otherFunctionName, currentFunction.getName()));
        options.add(String.format("%s <<<<< %s", otherFunctionName, currentFunction.getName()));

        String selection = askChoice("Search Direction", "Which direction would you like to search?", options, options.get(0));
        int direction = 1;
        if(selection.equals(options.get(1))) {
            direction = -1;
            println(String.format("Searching from %s -> %s", currentFunction.getName(), otherFunctionName));
        } else {
            println(String.format("Searching from %s -> %s", otherFunctionName, currentFunction.getName()));
        }

        ReferenceManager referenceManager = currentProgram.getReferenceManager();
        FunctionManager functionManager = currentProgram.getFunctionManager();

        SymbolTable symbolTable = currentProgram.getSymbolTable();

        SymbolIterator symbolIterator = symbolTable.getSymbols(otherFunctionName);

        if (symbolIterator == null) {
            println(String.format("Failed to get symbol iterator for: %s", otherFunctionName));
            return;
        }
        LinkedList<Pair<Symbol, Integer>> foundParents = new LinkedList<>();
        for (Symbol s :
                symbolTable.getSymbols(otherFunctionName)) {
            Address symAddr = s.getAddress();
            println(String.format("Checking %s @ %s", s.getName(), symAddr.toString()));
            Function otherFunction = functionManager.getFunctionAt(symAddr);

            Function parent, child;

            if (direction > 0) {
                child = currentFunction;
                parent = otherFunction;
            } else {
                child = otherFunction;
                parent = currentFunction;
            }

            int callOrder = CheckCallOrder(parent, child, referenceManager, functionManager);
            if (callOrder < 0) {
                println("No connection...");
                continue;
            }
            println(String.format("Found %dth order connection", callOrder));
            foundParents.add(new Pair<Symbol, Integer>(s, callOrder));
        }
    }

    private class SearchNode {
        SearchNode prev;
        int order;
        Function func;

        SearchNode(Function func) {
            prev = null;
            order = 0;
            this.func = func;
        }

        SearchNode(SearchNode prev, Function func) {
            this.prev = prev;
            this.order = prev.order + 1;
            this.func = func;
        }

        void printLineage() {
            int depth = 0;
            for(SearchNode curNode = this; curNode != null; curNode = curNode.prev) {
                print(String.format("%3d: ", depth));
                for (int i = 0; i < depth; i++) {
                    print("    ");
                }

                print(curNode.func.getName());
                if(curNode.prev != null) {
                    print("  ->");
                }
                print("\n");
                depth++;
            }
        }

    }

    public int CheckCallOrder(Function parent, Function child, ReferenceManager referenceManager, FunctionManager functionManager) {
        if (parent == child) {
            return 0;
        }
        /*
        Here's the search we're going to run... Since we're looking for a minimized connection here...
        (Technically, this search can be done in such a way as to find all possible routes between two functions)
        We're going to be doing a breadth first search. That way we don't have to deal with re-parenting or any such
        nonsense.
         */
        Set<Function> visited = new HashSet<Function>();
        Queue<SearchNode> frontier = new LinkedList<>();

        frontier.add(new SearchNode(child));

        while(!frontier.isEmpty()) {
            SearchNode curNode = frontier.remove();
            /*
            We don't check for stopping initially because we'll break early for search logic reasons. Don't want to keep
            looking if we already enqueued the right answer, so we'll use that. We've already checked the initial node
            for a zero length path...
             */
            for (Reference ref :
                    referenceManager.getReferencesTo(curNode.func.getEntryPoint())) {
                Function caller = functionManager.getFunctionContaining(ref.getFromAddress());
                if(caller == null) {
                    println(String.format("Bad reference: %s", ref.toString()));
                    continue;
                }

                if (caller == parent) {
                    SearchNode newNode = new SearchNode(curNode, caller);
                    newNode.printLineage();
                    return newNode.order;
                }

                frontier.add(new SearchNode(curNode, caller));
            }
        }

        return -1;
    }

}
