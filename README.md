# rm-ghidra
This is my own personal repository for ghidra scripts that I found useful.

Generally, these will be quick and dirty, one-off style,
hacky scripts that accomplish something specific that I was
looking for at some point.

Take for example [IsReachableFrom](./ghidra_scripts/IsReachableFrom.java):
This script will take the current location and another symbol name
and check if one is theoretically callable from the other using
a naive xref-based search.

# License _(Or lack thereof)_

This code is released without license, I would say
"dedicated to the public domain" but that implies this
is worth taking. The code in this repo is in the public domain and
free to be used however, wherever, by whomever, for whatever, with no guarantees.

No credits needed, treat this as you would code snippets found on stack overflow, up to
and including verifying any solutions you find in here.